# DiscoNode

DiscoNode is a small and lightweight wrapper for bot creation with the [discord.js](https://discord.js.org) framework.

## Installation

```bash
npm install --save disconode
```

## Example usage

```javascript
const DiscoNode = require('disconode');

let disco = new DiscoNode.Disco('YOUR_TOKEN');

disco.login().then(function() {
    console.log('Logged in.');
});

disco.registerCommand('ping', function(message) {
    message.soft.reply('pong');
}, 'Replies with pong.');

disco.setHelpCommand('help', '**Help Page**', true);
```

## Links

- [Documentation](https://gitlab.com/milan44/disconode/blob/master/docs/Disco.md)
- [GitLab](https://gitlab.com/milan44/disconode/)
- [discord.js](https://discord.js.org)