const Discord = require('discord.js');
const MessageHandler = require('./MessageHandler');

module.exports = class Disco {
    constructor(token, verbose) {
        this.token = token;
        this.verbose = verbose;

        this.connection = null;
        this.commands = {};
        this.helpCommand = null;
        this.messageHandler = null;
    }

    login() {
        let _this = this;

        return new Promise(function(resolve, reject) {
            _this.connection = new Discord.Client();
            _this.connection.on('ready', resolve);

            _this.messageHandler = new MessageHandler(_this.connection, _this);

            _this.connection.on('message', async function(message) {
                try {
                    await _this.messageHandler.handleCreate(message);
                } catch(e) {
                    if (_this.verbose) {
                        console.error(e);
                    }
                }
            });

            _this.connection.login(_this.token).catch(reject);
        });
    }

    on(event, callback) {
        let _this = this;

        if (Disco.isChannelEvent(event)) {
            this.connection.on(event, function(...args) {
                args = _this.prepareChannelEvent(event, args);

                callback(...args);
            });
        } else if (Disco.isMessageEvent(event)) {
            this.connection.on(event, function(...args) {
                args = _this.prepareMessageEvent(event, args);

                callback(...args);
            });
        } else {
            this.connection.on(event, callback);
        }
    }

    prepareMessageEvent(event, args) {
        switch(event) {
            case 'message':
            case 'messageDelete':
            case 'messageReactionRemoveAll':
                args[0] = this.messageHandler.extendMessage(args[0]);
                break;
            case 'messageUpdate':
                args[0] = this.messageHandler.extendMessage(args[0]);
                args[1] = this.messageHandler.extendMessage(args[1]);
                break;
            case 'messageDeleteBulk':
                for (const key in args[0]) {
                    if (!args[0].hasOwnProperty(key)) continue;

                    args[0][key] = this.messageHandler.extendMessage(args[0][key]);
                }
                break;

        }
        return args;
    }

    prepareChannelEvent(event, args) {
        switch(event) {
            case 'channelCreate':
            case 'channelDelete':
            case 'channelPinsUpdate':
                args[0] = this.messageHandler.extendChannel(args[0]);
                break;
            case 'channelUpdate':
                args[0] = this.messageHandler.extendChannel(args[0]);
                args[1] = this.messageHandler.extendChannel(args[1]);
                break;
        }
        return args;
    }

    static isChannelEvent(event) {
        switch(event) {
            case 'channelCreate':
            case 'channelDelete':
            case 'channelPinsUpdate':
            case 'channelUpdate':
                return true;
        }
        return false;
    }
    static isMessageEvent(event) {
        switch(event) {
            case 'message':
            case 'messageDelete':
            case 'messageDeleteBulk':
            case 'messageReactionRemoveAll':
            case 'messageUpdate':
                return true;
        }
        return false;
    }

    registerCommand(command, callback, description, customHelpFolder) {
		if (!customHelpFolder) {
			customHelpFolder = 'default';
		}
        this.commands[command] = {
            callback: callback,
            description: description ? description : 'No description provided.',
			folder: customHelpFolder
        };
    }

    setHelpCommand(command, title, sendAsEmbed) {
        this.helpCommand = {
            command: command,
            title: title,
            embed: sendAsEmbed
        };
    }
    getHelpCommand() {
        return this.helpCommand;
    }

    getCommand(text) {
        text = text.trim().toLowerCase();

        if (text in this.commands) {
            return this.commands[text].callback;
        }
        return false;
    }

    static chunkifyMessage(content, chunkPrefix, chunkSuffix) {
        let messages = [];
        let message = "";

        const characters = Array.from(content);
        for (let x=0;x<characters.length;x++) {
            message += characters[x];

            if (message.length > 1800) {
                message = message.trim();
                if (chunkPrefix) {
                    message = chunkPrefix + message;
                }
                if (chunkSuffix) {
                    message += chunkSuffix;
                }
                messages.push(message);

                message = "";
            }
        }

        if (message.length > 0) {
            message = message.trim();
            if (chunkPrefix) {
                message = chunkPrefix + message;
            }
            if (chunkSuffix) {
                message += chunkSuffix;
            }
            messages.push(message);
        }

        return messages;
    }

    buildHelp(asEmbed, customHelpFolder) {
		if (!customHelpFolder) {
			customHelpFolder = 'default';
		}
        if (asEmbed) {
            let embeds = [];
            let embed = new Discord.RichEmbed();

            let index = 0;

            for (const command in this.commands) {
                if (!this.commands.hasOwnProperty(command)) continue;

                const options = this.commands[command];
				
				if (options.folder !== customHelpFolder) {
					continue;
				}

                embed.addField(command, options.description);

                index++;
                if (index >= 25) {
                    embeds.push(embed);
                    embed = new Discord.RichEmbed();
                    index = 0;
                }
            }
            if (index > 0) {
                embeds.push(embed);
            }

            return embeds;
        } else {
            let padding = 0;
            for (const command in this.commands) {
                if (!this.commands.hasOwnProperty(command)) continue;

                if (command.length > padding) {
                    padding = command.length;
                }
            }

            padding += 2;

            let messages = [];
            let message = "";

            for (const command in this.commands) {
                if (!this.commands.hasOwnProperty(command)) continue;

                const options = this.commands[command];

                message += command.padEnd(padding, ' ') + options.description + "\n";

                if (message.length > 1800) {
                    messages.push("```\n" + message.trim() + "\n```");
                    message = "";
                }
            }
            if (message.length > 0) {
                messages.push("```\n" + message.trim() + "\n```");
            }

            return messages;
        }
    }
};