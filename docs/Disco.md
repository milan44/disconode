# Disco

The main hub for interacting with the Discord API, and the starting point for any bot.

## Constructor

```javascript
new DiscoNode.Disco(token, [verbose]);
```

|Parameter|Type|Optional|Default|Description|
|--|--|--|--|--|
|token|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|no|*none*|Authorization token for the logged in user/bot|
|verbose|[boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)|yes|*false*|If errors should be logged to the console|

## Properties

- [token](#token)
- [verbose](#verbose)
- [connection](#connection)

## Methods

- [login](#login)
- [on](#on)
- [registerCommand](#registercommand)
- [setHelpCommand](#sethelpcommand)
- [chunkifyMessage](#chunkifymessage)
- [buildHelp](#buildhelp)

---

### token

Authorization token for the logged in user/bot

**Type:** ?[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)

---

### verbose

If errors should be logged to the console

**Type:** ?[boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)

---

### connection

The `Discord.Client` used to connect to Discords API

**Type:** ?[Discord.Client](https://discord.js.org/#/docs/main/stable/class/Client)

---

### login

Logs the client in, establishing a websocket connection to Discord

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)

**Examples:**
```javascript
disco.login()
	.then(console.log)
	.catch(console.error);
```

---

### on

Binds an event handler to the discortd.js client object

|Parameter|Type|Optional|Description|
|--|--|--|--|
|event|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|no|The event to bind|
|callback|[Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)|no|The callback for that event|

**Returns:** [void](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined)

**Examples:**
```javascript
disco.on('error', function(error) {
	console.log('An error occured!');
});
```

---

### registerCommand

Registers a function that should be called when a specific command is sent as a message

|Parameter|Type|Optional|Description|
|--|--|--|--|
|command|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|no|The command|
|callback|[Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)|no|The callback for that command|
|description|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|yes|The description that will be used in the help command|
|customHelpFolder|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|yes|The "folder" in which the command should be saved|

**Returns:** [void](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined)

**Examples:**
```javascript
disco.registerCommand(';ping', function(message) {
	message.soft.reply('Pong!');
}, 'Replies with pong.');
```

---

### setHelpCommand

Sets the command which will generate a help message

|Parameter|Type|Optional|Description|
|--|--|--|--|
|command|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|no|The help command|
|title|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|yes|The title for the help message|
|sendAsEmbed|[boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)|yes|If the help message should be sent as a RichEmbed|

**Returns:** [void](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined)

**Examples:**
```javascript
disco.setHelpCommand(';help', '**Help Page**', true);
```

---

### chunkifyMessage

Makes sure a message is not longer than 2000 characters and breaks it up into multiple ones if necessary

|Parameter|Type|Optional|Description|
|--|--|--|--|
|content|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|no|The message|
|chunkPrefix|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|yes|This will be prepended on each chunk|
|chunkSuffix|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|yes|This will be appended on each chunk|

**Returns:** [Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)< [string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) >

**Examples:**
```javascript
const response = disco.chunkifyMessage('A very long message', "```\n", "\n```");
await message.channel.soft.sendBulk(response);
```

---

### buildHelp

Makes sure a message is not longer than 2000 characters and breaks it up into multiple ones if necessary

|Parameter|Type|Optional|Description|
|--|--|--|--|
|asEmbed|[boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)|yes|If the help message should be generated as RichEmbeds|
|customHelpFolder|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)|yes|Only show commands from this "folder"|

**Returns:** [Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)< [string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) > or [Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)< [RichEmbed](https://discord.js.org/#/docs/main/stable/class/RichEmbed) >

**Examples:**
```javascript
const help = disco.buildHelp(true);
await message.channel.soft.sendBulk(help);
```