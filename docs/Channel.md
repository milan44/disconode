# Message

When using the `DiscoNode.Disco.on` function to bind events, every message object will be extended with useful functions and properties.

**Type:** [Discord.TextChannel](https://discord.js.org/#/docs/main/stable/class/TextChannel)

## Methods

- [sendBulk](#sendbulk)
- [soft.send](#softsend)
- [soft.sendBulk](#softsendbulk)

---

### sendBulk

Send a bunch of message to this channel. This will not throw an error if it fails

|Parameter|Type|Optional|Description|
|--|--|--|--|
|messages|[Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)< [StringResolvable](https://discord.js.org/#/docs/main/stable/typedef/StringResolvable) &#124; [RichEmbed](https://discord.js.org/#/docs/main/stable/class/RichEmbed) >|no|The messages to be sent|

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)< [Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)< [Message](Message.md) > >

**Examples:**
```javascript
const myMessages = [
	'These are',
	'multiple messages',
	'sent via the',
	'sendBulk command.'
];
channel.sendBulk(myMessages).then(function(messages) {
	// Handle messages
}).catch(console.error);
```

---

### soft.send

Send a message to this channel. This will not throw an error if it fails

|Parameter|Type|Optional|Description|
|--|--|--|--|
|content|[StringResolvable](https://discord.js.org/#/docs/main/stable/typedef/StringResolvable)|no|The content for the message|
|options|[MessageOptions](https://discord.js.org/#/docs/main/stable/typedef/MessageOptions)|yes|The options to provide|

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)< ([Message](Message.md) | [Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)< [Message](https://discord.js.org/#/docs/main/stable/class/Message) >) >

**Examples:**
```javascript
channel.soft.send('Hey, I\'m a reply!')
	.then(function(sent) {
		console.log(`Sent a message in ${sent.channel.name}`);
	});
```

---

### soft.sendBulk

Same as [sendBulk](#sendbulk) but this one wont throw an error.