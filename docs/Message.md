# Message

When using the `DiscoNode.Disco.on` function to bind events, every message object will be extended with useful functions and properties.

**Type:** [Discord.Message](https://discord.js.org/#/docs/main/stable/class/Message)

## Properties

- [isSelf](#isself)
- [isDM](#isdm)
- [comparableContent](#comparablecontent)
- [command](#command)
- [commandFreeContent](#commandfreecontent)
- [channel](#channel)

## Methods

- [soft.delete](#softdelete)
- [soft.pin](#softpin)
- [soft.edit](#softedit)
- [soft.reply](#softreply)
- [soft.react](#softreact)
- [soft.clearReactions](#softclearreactions)

---

### isSelf

If the message was sent by the bot himself

**Type:** [boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)

---

### isDM

If the message was sent via Direct Messages

**Type:** [boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)

---

### comparableContent

The content of the message, trimmed and in lowercase

**Type:** [string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)

---

### command

Everything until the first space in the content of the message

**Type:** [string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)

---

### commandFreeContent

The content of the message without the [command](#command)

**Type:** [string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)

---

### channel

The channel the message was sent in.

**Type:** [Disco.Channel](Channel.md)

---

### soft.delete

Deletes the message. This will not throw an error if it fails

|Parameter|Type|Optional|Description|
|--|--|--|--|
|timeout|[number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)|yes|How long to wait to delete the message in milliseconds|

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)< [Message](Message.md) >

**Examples:**
```javascript
message.soft.delete()
	.then(function(msg) {
		console.log(`Deleted message from ${msg.author.username}`);
	});
```

---

### soft.pin

Pins this message to the channel's pinned messages. This will not throw an error if it fails

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)< [Message](Message.md) >

**Examples:**
```javascript
message.soft.pin()
	.then(function(msg) {
		console.log(`Pinned message from ${msg.author.username}`);
	});
```

---

### soft.edit

Edit the content of the message. This will not throw an error if it fails

|Parameter|Type|Optional|Description|
|--|--|--|--|
|content|[StringResolvable](https://discord.js.org/#/docs/main/stable/typedef/StringResolvable)|no|The new content for the message|
|options|[MessageEditOptions](https://discord.js.org/#/docs/main/stable/typedef/MessageEditOptions) or [RichEmbed](https://discord.js.org/#/docs/main/stable/class/RichEmbed)|yes|The options to provide|

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)< [Message](Message.md) >

**Examples:**
```javascript
message.soft.edit('This is my new content!')
	.then(function(msg) {
		console.log(`New message content: ${msg}`);
	});
```

---

### soft.reply

Reply to the message. This will not throw an error if it fails

|Parameter|Type|Optional|Description|
|--|--|--|--|
|content|[StringResolvable](https://discord.js.org/#/docs/main/stable/typedef/StringResolvable)|no|The new content for the message|
|options|[MessageOptions](https://discord.js.org/#/docs/main/stable/typedef/MessageOptions)|yes|The options to provide|

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)< ([Message](Message.md) | [Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)< [Message](https://discord.js.org/#/docs/main/stable/class/Message) >) >

**Examples:**
```javascript
message.soft.reply('Hey, I\'m a reply!')
	.then(function(sent) {
		console.log(`Sent a reply to ${sent.author.username}`);
	});
```

---

### soft.react

Add a reaction to the message. This will not throw an error if it fails

|Parameter|Type|Optional|Description|
|--|--|--|--|
|emoji|[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) or [Emoji](https://discord.js.org/#/docs/main/stable/class/Emoji) or [ReactionEmoji](https://discord.js.org/#/docs/main/stable/class/ReactionEmoji)|no|The emoji to react with|

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)< [MessageReaction](https://discord.js.org/#/docs/main/stable/class/MessageReaction) >

**Examples:**
```javascript
message.soft.react(message.guild.emojis.get('123456789012345678'))
	.then(console.log);
```

---

### soft.clearReactions

Remove all reactions from a message. This will not throw an error if it fails

**Returns:** [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)< [Message](Message.md) >

**Examples:**
```javascript
await message.soft.clearReactions();
```