module.exports = class MessageHandler {
    constructor(client, disco) {
        this.client = client;
        this.disco = disco;
    }

    async handleCreate(message) {
        message = this.extendMessage(message);

        const command = this.disco.getCommand(message.command);
        const helpCommand = this.disco.getHelpCommand();

        if (command && !message.isSelf) {
            command(message);
        } else if (helpCommand !== null && message.command === helpCommand.command && !message.isSelf) {
            let help = this.disco.buildHelp(helpCommand.embed);
            if (helpCommand.title) {
                help.unshift(helpCommand.title);
            }
            await message.channel.soft.sendBulk(help);
        }
    }

    static escapeRegExp(string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }

    extendMessage(message) {
        let _this = this;

        message.isSelf = message.author.id === this.client.user.id;
        message.isDM = message.channel.type === 'dm';

        message.comparableContent = message.content.trim().toLowerCase();
        message.command = message.comparableContent.split(' ')[0];

        const regexp = new RegExp('^' + MessageHandler.escapeRegExp(message.command), 'ig');

        message.commandFreeContent = message.content.replace(regexp, '').trim();

        message.soft = {};

        message.soft.delete = async function(timeout) {
            try {
                if (!message.deletable) {
                    return message;
                }
                let msg = await message.delete(timeout);
                msg = _this.extendMessage(msg);
                return msg;
            } catch(e) {
                if (_this.disco.verbose) {
                    console.error(e);
                }
                return false;
            }
        };
        message.soft.pin = async function() {
            try {
                if (message.pinned || !message.pinnable) {
                    return message;
                }
                let msg = await message.pin();
                msg = _this.extendMessage(msg);
                return msg;
            } catch(e) {
                if (_this.disco.verbose) {
                    console.error(e);
                }
                return false;
            }
        };
        message.soft.edit = async function(content, options) {
            try {
                if (!message.editable) {
                    return message;
                }
                let msg = await message.edit(content, options);
                msg = _this.extendMessage(msg);
                return msg;
            } catch(e) {
                if (_this.disco.verbose) {
                    console.error(e);
                }
                return false;
            }
        };
        message.soft.reply = async function(content, options) {
            try {
                let msg = await message.reply(content, options);
                msg = _this.extendMessage(msg);
                return msg;
            } catch(e) {
                if (_this.disco.verbose) {
                    console.error(e);
                }
                return false;
            }
        };
        message.soft.react = async function(emoji) {
            try {
                let msg = await message.react(emoji);
                msg = _this.extendMessage(msg);
                return msg;
            } catch(e) {
                if (_this.disco.verbose) {
                    console.error(e);
                }
                return false;
            }
        };
        message.soft.clearReactions = async function() {
            try {
                let msg = await message.clearReactions();
                msg = _this.extendMessage(msg);
                return msg;
            } catch(e) {
                if (_this.disco.verbose) {
                    console.error(e);
                }
                return false;
            }
        };

        message.channel = this.extendChannel(message.channel);

        return message;
    }

    extendChannel(channel) {
        let _this = this;

        channel.sendBulk = async function(messages) {
            let msgs = [];
            for (let x=0;x<messages.length;x++) {
                msgs.push(_this.extendMessage(await channel.send(messages[x])));
            }
            return msgs;
        };

        channel.soft = {};

        channel.soft.send = async function(content, options) {
            try {
                let message = await channel.send(content, options);
                message = _this.extendMessage(message);
                return message;
            } catch(e) {
                if (_this.disco.verbose) {
                    console.error(e);
                }
                return false;
            }
        };
        channel.soft.sendBulk = async function(messages) {
            try {
                return await channel.sendBulk(messages);
            } catch(e) {
                if (_this.disco.verbose) {
                    console.error(e);
                }
                return false;
            }
        };

        return channel;
    }
};